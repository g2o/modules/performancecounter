# PerformanceCounter

## Introduction

The main motivation behind this project is to give the ability to time certain algorithms and evaluate how it performed in **squirrel**.  
This module is shared, which means that you can load it not only on **client-side** but, also on **server-side** without no issues.

## Documentation

https://g2o.gitlab.io/modules/PerformanceCounter