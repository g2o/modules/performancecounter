#ifndef _PERFORMANCE_COUNTER_H
#define _PERFORMANCE_COUNTER_H

#include <chrono>
#include <sqapi.h>

#ifdef _WIN32
#pragma warning(disable:4244) // conversion from __int64 to int32, possible loss of data
#endif

enum PrecisionUnit
{
	NANOSECONDS,
	MICROSECONDS,
	MILISECONDS
};

class PerformanceCounter
{
private:
	std::chrono::time_point<std::chrono::high_resolution_clock> startTime;
	std::chrono::time_point<std::chrono::high_resolution_clock> stopTime;

public:
	void start()
	{
		startTime = std::chrono::high_resolution_clock::now();
	}

	void stop()
	{
		stopTime = std::chrono::high_resolution_clock::now();
	}

	SQInteger count(SQInteger precision)
	{
		switch (precision)
		{
			case NANOSECONDS:
				return std::chrono::duration_cast<std::chrono::nanoseconds>(stopTime - startTime).count();

			case MICROSECONDS:
				return std::chrono::duration_cast<std::chrono::microseconds>(stopTime - startTime).count();

			case MILISECONDS:
				return std::chrono::duration_cast<std::chrono::milliseconds>(stopTime - startTime).count();

			default:
				return -1;
		}
	}
};

#endif