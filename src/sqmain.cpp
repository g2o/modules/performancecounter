#include <sqapi.h>
#include "PerformanceCounter.h"

extern "C" SQRESULT SQRAT_API sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
	SqModule::Initialize(vm, api);

	Sqrat::ConstTable(vm).Enum("PrecisionUnit", Sqrat::Enumeration(vm)
		/* squirreldoc (const)
		*
		* Represents nanoseconds precision.
		*
		* @category PrecisionUnit
		* @side		shared
		* @name		PrecisionUnit::NANOSECONDS
		*
		*/
		.Const("NANOSECONDS", PrecisionUnit::NANOSECONDS)

		/* squirreldoc (const)
		*
		* Represents microseconds precision.
		*
		* @category PrecisionUnit
		* @side		shared
		* @name		PrecisionUnit::MICROSECONDS
		*
		*/
		.Const("MICROSECONDS", PrecisionUnit::MICROSECONDS)
		/* squirreldoc (const)
		*
		* Represents miliseconds precision.
		*
		* @category PrecisionUnit
		* @side		shared
		* @name		PrecisionUnit::MILISECONDS
		*
		*/
		.Const("MILISECONDS", PrecisionUnit::MILISECONDS)
	);

	/* squirreldoc (class)
	*
	* This class represents performance timer that can measure code execution time.
	*
	* @side		shared
	* @name		PerformanceCounter
	*
	*/
	Sqrat::RootTable(vm).Bind("PerformanceCounter", Sqrat::Class<PerformanceCounter>(vm, "PerformanceCounter")

		/* squirreldoc (constructor)
		*
		*
		*/
		.Ctor()
		/* squirreldoc (method)
		*
		* This method starts performance counter.
		*
		* @name     start
		*
		*/
		.Func("start", &PerformanceCounter::start)
		/* squirreldoc (method)
		*
		* This method stops performance counter.
		*
		* @name     stop
		*
		*/
		.Func("stop", &PerformanceCounter::stop)
		/* squirreldoc (method)
		*
		* This method returns the time passed between start & stop calls.
		*
		* @name     count
		* @param	(PrecisionUnit) precision the precision unit that will be used to count the elapsed time.
		* @return	(int) the elapsed in passed precision unit.
		*
		*/
		.Func("count", &PerformanceCounter::count)
		/* squirreldoc (method)
		*
		* Alis for [count](#count) method.
		*
		* @name     duration
		* @param	(PrecisionUnit) precision the precision unit that will be used to count the elapsed time.
		* @return	(int) the elapsed in passed precision unit.
		*
		*/
		.Func("duration", &PerformanceCounter::count)
	);

	return SQ_OK;
}
